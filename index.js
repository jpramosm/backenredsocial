const mongoose = require('mongoose');
const app = require('./app');
const port = 3000 || process.env.PORT;

app.listen(port,()=>{
    console.log('Serve online in port',port);
})

mongoose.connect('mongodb://localhost:27017/redSocial', { useNewUrlParser: true, useUnifiedTopology:true })
    .then(res => console.log('Estas conectado la base de datos'))
    .catch(err => console.log('Error'))
