const express = require('express');
const bcrypt = require('bcryptjs');
const services  = require('../services/jwt');
const User = require('../models/user.models');
const middlewares = require('../middlewares/autentication')
const router = express.Router();

router.post('/user', (req, res) => {
    const contenido = req.body;

    const user = new User({
        name: contenido.name,
        surname: contenido.surname,
        nick: contenido.nick,
        email: contenido.email,
        password: bcrypt.hashSync(contenido.password)
    });

    user.save((err, data) => {
        if (err) {
            res.status(500).json(err)
        } else {
            res.status(200).json(data)
        };
    });
});

router.post('/user/login' , (req, res) => {
    const contenido = req.body;

    User.findOne({ email: contenido.email }, (err, data) => {
        if (err) {
            return res.status(500).json(err);
        }
        if (!data) {
            return res.status(500).json({ message: 'User not found' });
        }
        if (!bcrypt.compareSync(contenido.password, data.password)) {
            return res.status(500).json({ message: 'Password not coincided' });
        } else {
            data.password = '********************';
            let token = services.token(data);
            res.status(200).json({data,token});
        };
    })
});


module.exports = router;



