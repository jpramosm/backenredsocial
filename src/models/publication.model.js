const mongoose = require('mongoose');
const schema = mongoose.Schema;

const publicationSchema = new schema({
    text: String,
    file: String,
    created_at: String,
    user: { type: schema.Types.ObjectId, ref: 'User' },
});

module.exports = mongoose.model('Publication',publicationSchema);