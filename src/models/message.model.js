const mongoose = require('mongoose');
const schema = mongoose.Schema;

const messageSchema = new schema({
    emmiter: { type: schema.Types.ObjectId, ref: 'User' },
    receiver: { type: schema.Types.ObjectId, ref: 'User' },
    text: String,
    created_at: String
});

module.exports = mongoose.model('Message',messageSchema);