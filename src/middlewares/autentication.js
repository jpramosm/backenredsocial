const jwt = require('jsonwebtoken');
let semilla = 'Mi_password';

let verifyToken = (req, res, next) => {
    let token = req.get('token');
    jwt.verify(token, semilla, (err, decode) => {
        if (err) {
            return res.status(500).json(err);
        } else {
            next();
        }
    });
};

module.exports = verifyToken;