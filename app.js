const express = require('express');
const bodyParser = require('body-parser');
const app = express();


//**********  Middlewares ***********//
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

//********** Rutas *********//
app.use(require('./src/routes/index.routes'));

//********** Exports ********** //
module.exports = app;